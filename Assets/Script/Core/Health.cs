using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace RPG.Core
{
    public class Health : MonoBehaviour
    {
        [SerializeField] private float currentHealth = 100f;
        private float maxHealth;
        private bool isDead;
        public bool IsDead { get { return isDead; } }

        private Animator animator;
        private ActionScheduler actionScheduler;

        private void Awake()
        {
            maxHealth = currentHealth;
            animator = transform.Find("model").GetComponent<Animator>();
            actionScheduler = GetComponent<ActionScheduler>();
        }

        public void TakeDamage(float damage)
        {
            currentHealth -= damage;
            if(currentHealth <= 0)
            {
                currentHealth = 0;
                Die();
            }
        }

        private void Die()
        {
            if (isDead) return;

            isDead = true;
            animator.SetTrigger("death");
            actionScheduler.CancelCurrentAction();
            //GetComponent<NavMeshAgent>().enabled = false;

            //Destroy(gameObject, 2f);
        }
    }

}
