using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Control;

namespace RPG.Core
{
    public class FollowCamera : MonoBehaviour
    {
        private Transform target;

        private void Awake()
        {
            target = FindObjectOfType<PlayerController>().transform;
        }

        // Update is called once per frame
        void LateUpdate()
        {
            transform.position = target.position;
        }
    }

}
