using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Movement;
using RPG.Combat;
using RPG.Core;

namespace RPG.Control 
{
    public class PlayerController : MonoBehaviour
    {
        public event Action OnInteractWithMouseButton;

        private Camera cameraMain;
        private Mover mover;
        private Fighter fighter;
        private Health health;

        private void Awake()
        {
            cameraMain = Camera.main;
            mover = GetComponent<Mover>();
            fighter = GetComponent<Fighter>();
            health = GetComponent<Health>();
        }

        private void Update()
        {
            if (health.IsDead) return;
            if (InteractWithCombat()) return;
            if(InteractWithMovement()) return;
            print("Nothing to do.");
        }

        private bool InteractWithCombat()
        {
            RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());

            foreach(RaycastHit hit in hits)
            {
                CombatTarget combatTarget = hit.collider.GetComponent<CombatTarget>();
                if (combatTarget == null) continue;

                if (!fighter.CanAttack(combatTarget.gameObject)) continue;

                if (Input.GetMouseButton(0))
                {
                    fighter.Attack(combatTarget.gameObject);
                }              
                return true;
            }
            return false;
        }

        private bool InteractWithMovement()
        {
            RaycastHit hit;
            bool hasHit = Physics.Raycast(GetMouseRay(), out hit);

            if (hasHit)
            {
                if (Input.GetMouseButton(0))
                {
                    mover.StartMoveAction(hit.point, 1f);
                    OnInteractWithMouseButton?.Invoke();
                }
                return true;
            }
            return false;
        }

        private Ray GetMouseRay()
        {
            return cameraMain.ScreenPointToRay(Input.mousePosition);
        }
    }
}

