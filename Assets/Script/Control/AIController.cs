using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Movement;
using RPG.Combat;
using RPG.Core;
using System;

namespace RPG.Control
{
    public class AIController : MonoBehaviour
    {
        [SerializeField] float chaseDistance = 5f;
        [SerializeField] float suspicionTimer = 3f;
        [SerializeField] float dwellTimer = 5f;
        [SerializeField] PatrolPath patrolPath;
        [SerializeField] float waypointTolerance = .3f;
        [SerializeField][Range(0,1)] float patrolSpeedFaction = 0.2f;

        private GameObject player;
        private Fighter fighter;
        private Mover mover;
        private Health health;
        private ActionScheduler actionScheduler;

        private Vector3 guardPosition;
        private int currentWaypointIndex = 0;
        private float timeSinceLastSawThePlayer = Mathf.Infinity;
        private float timeSinceArriveAtWaypoint = Mathf.Infinity;


        private void Awake()
        {
            fighter = GetComponent<Fighter>();
            mover = GetComponent<Mover>();
            health = GetComponent<Health>();
            actionScheduler = GetComponent<ActionScheduler>();
        }

        private void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player");
            guardPosition = transform.position;
            
        }

        private void Update()
        {
            // ------------My Solution to Finding the Player------------------

            //Collider[] colliderArray = Physics.OverlapSphere(transform.position, chaseDistance);

            //foreach(Collider collider in colliderArray)
            //{
            //    if (collider.tag != "Player") continue;

            //    GameObject player = collider.gameObject;
            //    if (InAttackRange(player))
            //    {
            //        fighter.Attack(player);
            //    }
            //    else
            //    {
            //        fighter.Cancel();
            //        mover.Cancel();
            //    }
            //}
            //-----------------------------------------------------------------
            if (health.IsDead) return;

            if (InAttackRangeOfPlayer(player) && fighter.CanAttack(player))
            {
                AttackBehaviour();
            }
            else if (timeSinceLastSawThePlayer <= suspicionTimer)
            {
                SuspicionBehaviour();
            }
            else
            {
                PatrolBehaviour();
            }

            UpdateTimers();
        }

        private void UpdateTimers()
        {
            timeSinceLastSawThePlayer += Time.deltaTime;
            timeSinceArriveAtWaypoint += Time.deltaTime;
        }

        private void PatrolBehaviour()
        {
            Vector3 nextPosition = guardPosition;

            if(patrolPath != null)
            {
                if (AtWaypoint())
                {
                    dwellTimer = UnityEngine.Random.Range(1f, 5f);
                    timeSinceArriveAtWaypoint = 0;
                    CycleWaypoint();
                }
                nextPosition = GetCurrentWaypoint();
            }
            
            if(timeSinceArriveAtWaypoint >= dwellTimer)
            {
                mover.StartMoveAction(nextPosition, patrolSpeedFaction);
            }
        }

        private bool AtWaypoint()
        {
            float distanceToWaypoint = Vector3.Distance(transform.position, GetCurrentWaypoint());
            return distanceToWaypoint < waypointTolerance;
        }

        private void CycleWaypoint()
        {
            currentWaypointIndex = patrolPath.GetNextIndex(currentWaypointIndex);
        }

        private Vector3 GetCurrentWaypoint()
        {
            return patrolPath.GetWaypointPosition(currentWaypointIndex);
        }

        private void SuspicionBehaviour()
        {
            actionScheduler.CancelCurrentAction();
        }


        private void AttackBehaviour()
        {
            timeSinceLastSawThePlayer = 0;
            fighter.Attack(player);
        }

        private bool InAttackRangeOfPlayer(GameObject player)
        {
            float distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
            return distanceToPlayer < chaseDistance;
        }

        //Called by Unity
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, chaseDistance);
        }
    }

}
