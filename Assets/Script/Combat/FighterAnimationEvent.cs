using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Combat;

public class FighterAnimationEvent : MonoBehaviour
{
    Fighter fighter;

    private void Awake()
    {
        fighter = transform.parent.GetComponent<Fighter>();
    }

    //Animation Event 
    void Hit()
    {
        Debug.Log("Hit");
        fighter.Hit();
    }
}
