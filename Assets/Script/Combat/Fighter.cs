using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using RPG.Movement;
using RPG.Core;


namespace RPG.Combat
{
    public class Fighter : MonoBehaviour, IAction
    {
        [SerializeField] float damage = 20f;
        [SerializeField] float weaponRange = 2f;
        [SerializeField] float timeBetweenAttacksMax = 1.5f;
        //[SerializeField] float turnSpeed = 2f;
        private float timeBetweenAttacks;

        private Transform target;
        private Health targetHealth;
        private float attackRand = 0;

        private Mover mover;
        private Animator animator;
        private ActionScheduler actionScheduler;

        private void Awake()
        {
            mover = GetComponent<Mover>();
            actionScheduler = GetComponent<ActionScheduler>();
            animator = transform.Find("model").GetComponent<Animator>();
        }


        private void Update()
        {
            timeBetweenAttacks -= Time.deltaTime;

            if (target != null)
            {
                if (targetHealth.IsDead) return;

                if (!GetIsInRange())
                {
                    mover.MoveTo(target.position, 1f);
                }  
                else
                {
                    mover.Cancel();
                    AttackBehaviour();

                }
            }
        }

        private bool GetAttackRate()
        {
            return timeBetweenAttacks <= 0;
        }

        private void AttackBehaviour()
        {
            Vector3 dirToTarget = Vector3.Normalize(target.position - this.transform.position);
            var dotProduct = Vector3.Dot(transform.forward, dirToTarget);
            Debug.Log(dotProduct);
            if(dotProduct > 0.96f)
            {
                if (GetAttackRate())
                {
                    RandomizeAttackAnimation();
                    TriggerAttack();
                    timeBetweenAttacks = timeBetweenAttacksMax;
                }
            }
            else
            {
                transform.LookAt(target);
            }

           
        }

        private void TriggerAttack()
        {
            animator.ResetTrigger("stopAttacking");
            animator.SetTrigger("attack"); //trigger Animation Event Hit(); -> FighterAnimationEvent.cs
        }

        private void RandomizeAttackAnimation()
        {
            attackRand = UnityEngine.Random.Range(0.0f, 1f);
            attackRand = Mathf.RoundToInt(attackRand);
            animator.SetFloat("attackRand", attackRand);
        }

        private bool GetIsInRange()
        {
            float distance = Vector3.Distance(target.position, this.transform.position);
            bool isInRange = distance < weaponRange;
            return isInRange;
        }

        private void SetTargetToNull()
        {
            if(target != null)
            {
                target = null;
                targetHealth = null;
            }
                
        }

        public bool CanAttack(GameObject combatTarget)
        {
            if (combatTarget == null) return false;

            Health targetToTest = combatTarget.GetComponent<Health>();
            return targetToTest != null && !targetToTest.IsDead;
        }

        public void Attack(GameObject combatTarget)
        {
            //print("Im Attacking: " + combatTarget.name);
            actionScheduler.StartAction(this);
            target = combatTarget.transform;
            targetHealth = target.GetComponent<Health>();
        }

        public void Cancel()
        {
            timeBetweenAttacks = 0;
            SetTargetToNull();
            mover.Cancel();
            StopAttack();
        }

        private void StopAttack()
        {
            animator.ResetTrigger("attack");
            animator.SetTrigger("stopAttacking");
        }

        public void Hit()
        {
            if (target == null) return;

            targetHealth.TakeDamage(damage);
        }
    }

}
