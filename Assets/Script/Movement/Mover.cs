using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using RPG.Core;

namespace RPG.Movement
{
    public class Mover : MonoBehaviour, IAction
    {
        [SerializeField] float maxSpeed = 6f;

        private NavMeshAgent agent;
        private Health health;
        private Animator anim;
        private Camera cameraMain;
        private ActionScheduler actionScheduler;

        private void Awake()
        {
            anim = transform.Find("model").GetComponent<Animator>();
            health = GetComponent<Health>();
            agent = GetComponent<NavMeshAgent>();
            actionScheduler = GetComponent<ActionScheduler>();
        }

        // Update is called once per frame
        void Update()
        {
            agent.enabled = !health.IsDead;

            UpdateAnimation();
        }

        private void UpdateAnimation()
        {
            Vector3 velocity = agent.velocity;
            Vector3 localVelocity = transform.InverseTransformDirection(velocity); //Change World Space to Local Space

            //Debug.Log("Velocity: " + velocity);
            //Debug.Log("LocalVelocity: " + localVelocity);
            float speed = localVelocity.z;
            anim.SetFloat("forwardSpeed", speed);
        }

        public void Cancel()
        {
            agent.isStopped = true;
        }

        public void StartMoveAction(Vector3 destination, float speedFraction)
        {
            actionScheduler.StartAction(this);
            MoveTo(destination, speedFraction);
        }
        

        public void MoveTo(Vector3 destination, float speedFraction)
        {
            agent.destination = destination;
            agent.speed = maxSpeed * Mathf.Clamp01(speedFraction);
            agent.isStopped = false;
        }
    }

}
