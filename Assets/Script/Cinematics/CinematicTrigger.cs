using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Control;
using UnityEngine.Playables;

namespace RPG.Cinematics
{
    public class CinematicTrigger : MonoBehaviour
    {
        private bool hasActivate = false;

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<PlayerController>() != null && !hasActivate)
            {
                GetComponent<PlayableDirector>().Play();
                hasActivate = true;

            }
        }
    }

}
